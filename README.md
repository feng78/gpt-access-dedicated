   <table>
            <tr>
              <td>
                <p>地市</p>
              </td>
              <td colspan="2">
                <p>农村社区</p>
              </td>
              <td colspan="2">
                <p>农村孕产妇</p>
              </td>
              <td colspan="2">
                <p>农村儿童</p>
              </td>
              <td colspan="2">
                <p>农村中小学生</p>
              </td>
              <td colspan="2">
                <p>城市社区</p>
              </td>
              <td colspan="2">
                <p>城市健康体检</p>
              </td>
              <td colspan="2">
                <p>城市孕产妇</p>
              </td>
              <td colspan="2">
                <p>城市儿童</p>
              </td>
              <td colspan="2">
                <p>城市中小学生</p>
              </td>
              <td colspan="2">
                <p>合计</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>菏泽</p>
              </td>
              <td>
                <p>101341</p>
              </td>
              <td colspan="2">
                <p>11684</p>
              </td>
              <td colspan="2">
                <p>3316</p>
              </td>
              <td colspan="2">
                <p>12560</p>
              </td>
              <td colspan="2">
                <p>56708</p>
              </td>
              <td colspan="2">
                <p>18338</p>
              </td>
              <td colspan="2">
                <p>15922</p>
              </td>
              <td colspan="2">
                <p>5374</p>
              </td>
              <td colspan="2">
                <p>13187</p>
              </td>
              <td colspan="2">
                <p>238430</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>聊城</p>
              </td>
              <td>
                <p>136919</p>
              </td>
              <td colspan="2">
                <p>2083</p>
              </td>
              <td colspan="2">
                <p>2083</p>
              </td>
              <td colspan="2">
                <p>11321</p>
              </td>
              <td colspan="2">
                <p>53773</p>
              </td>
              <td colspan="2">
                <p>25831</p>
              </td>
              <td colspan="2">
                <p>3500</p>
              </td>
              <td colspan="2">
                <p>3500</p>
              </td>
              <td colspan="2">
                <p>11885</p>
              </td>
              <td colspan="2">
                <p>250895</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>德州</p>
              </td>
              <td>
                <p>161725</p>
              </td>
              <td colspan="2">
                <p>2140</p>
              </td>
              <td colspan="2">
                <p>8088</p>
              </td>
              <td colspan="2">
                <p>10293</p>
              </td>
              <td colspan="2">
                <p>103786</p>
              </td>
              <td colspan="2">
                <p>18485</p>
              </td>
              <td colspan="2">
                <p>3000</p>
              </td>
              <td colspan="2">
                <p>3000</p>
              </td>
              <td colspan="2">
                <p>10886</p>
              </td>
              <td colspan="2">
                <p>321403</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>济宁</p>
              </td>
              <td>
                <p>166211</p>
              </td>
              <td colspan="2">
                <p>27562</p>
              </td>
              <td colspan="2">
                <p>31957</p>
              </td>
              <td colspan="2">
                <p>20664</p>
              </td>
              <td colspan="2">
                <p>54640</p>
              </td>
              <td colspan="2">
                <p>83183</p>
              </td>
              <td colspan="2">
                <p>3000</p>
              </td>
              <td colspan="2">
                <p>3000</p>
              </td>
              <td colspan="2">
                <p>10022</p>
              </td>
              <td colspan="2">
                <p>400239</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>枣庄</p>
              </td>
              <td>
                <p>92860</p>
              </td>
              <td colspan="2">
                <p>6224</p>
              </td>
              <td colspan="2">
                <p>8155</p>
              </td>
              <td colspan="2">
                <p>10825</p>
              </td>
              <td colspan="2">
                <p>50513</p>
              </td>
              <td colspan="2">
                <p>955</p>
              </td>
              <td colspan="2">
                <p>3000</p>
              </td>
              <td colspan="2">
                <p>3000</p>
              </td>
              <td colspan="2">
                <p>11595</p>
              </td>
              <td colspan="2">
                <p>187127</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>泰安</p>
              </td>
              <td>
                <p>61917</p>
              </td>
              <td colspan="2">
                <p>17481</p>
              </td>
              <td colspan="2">
                <p>15563</p>
              </td>
              <td colspan="2">
                <p>10831</p>
              </td>
              <td colspan="2">
                <p>64923</p>
              </td>
              <td colspan="2">
                <p>21640</p>
              </td>
              <td colspan="2">
                <p>8078</p>
              </td>
              <td colspan="2">
                <p>4047</p>
              </td>
              <td colspan="2">
                <p>12735</p>
              </td>
              <td colspan="2">
                <p>217215</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>莱芜</p>
              </td>
              <td>
                <p>60073</p>
              </td>
              <td colspan="2">
                <p>5207</p>
              </td>
              <td colspan="2">
                <p>4569</p>
              </td>
              <td colspan="2">
                <p>41478</p>
              </td>
              <td colspan="2">
                <p>44095</p>
              </td>
              <td colspan="2">
                <p>16562</p>
              </td>
              <td colspan="2">
                <p>2793</p>
              </td>
              <td colspan="2">
                <p>1878</p>
              </td>
              <td colspan="2">
                <p>40136</p>
              </td>
              <td colspan="2">
                <p>216792</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>济南</p>
              </td>
              <td>
                <p>113425</p>
              </td>
              <td colspan="2">
                <p>2808</p>
              </td>
              <td colspan="2">
                <p>2839</p>
              </td>
              <td colspan="2">
                <p>11454</p>
              </td>
              <td colspan="2">
                <p>88102</p>
              </td>
              <td colspan="2">
                <p>40950</p>
              </td>
              <td colspan="2">
                <p>4402</p>
              </td>
              <td colspan="2">
                <p>4142</p>
              </td>
              <td colspan="2">
                <p>22256</p>
              </td>
              <td colspan="2">
                <p>290378</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>临沂</p>
              </td>
              <td>
                <p>124874</p>
              </td>
              <td colspan="2">
                <p>18160</p>
              </td>
              <td colspan="2">
                <p>9296</p>
              </td>
              <td colspan="2">
                <p>19193</p>
              </td>
              <td colspan="2">
                <p>67766</p>
              </td>
              <td colspan="2">
                <p>32509</p>
              </td>
              <td colspan="2">
                <p>8656</p>
              </td>
              <td colspan="2">
                <p>8082</p>
              </td>
              <td colspan="2">
                <p>10660</p>
              </td>
              <td colspan="2">
                <p>299196</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>淄博</p>
              </td>
              <td>
                <p>81799</p>
              </td>
              <td colspan="2">
                <p>3705</p>
              </td>
              <td colspan="2">
                <p>3705</p>
              </td>
              <td colspan="2">
                <p>10102</p>
              </td>
              <td colspan="2">
                <p>87147</p>
              </td>
              <td colspan="2">
                <p>23977</p>
              </td>
              <td colspan="2">
                <p>4081</p>
              </td>
              <td colspan="2">
                <p>4081</p>
              </td>
              <td colspan="2">
                <p>10102</p>
              </td>
              <td colspan="2">
                <p>228699</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>潍坊</p>
              </td>
              <td>
                <p>49497</p>
              </td>
              <td colspan="2">
                <p>19385</p>
              </td>
              <td colspan="2">
                <p>17615</p>
              </td>
              <td colspan="2">
                <p>10056</p>
              </td>
              <td colspan="2">
                <p>72991</p>
              </td>
              <td colspan="2">
                <p>25611</p>
              </td>
              <td colspan="2">
                <p>3999</p>
              </td>
              <td colspan="2">
                <p>3000</p>
              </td>
              <td colspan="2">
                <p>10160</p>
              </td>
              <td colspan="2">
                <p>212314</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>滨州</p>
              </td>
              <td>
                <p>119142</p>
              </td>
              <td colspan="2">
                <p>15894</p>
              </td>
              <td colspan="2">
                <p>15894</p>
              </td>
              <td colspan="2">
                <p>10392</p>
              </td>
              <td colspan="2">
                <p>62197</p>
              </td>
              <td colspan="2">
                <p>20691</p>
              </td>
              <td colspan="2">
                <p>6000</p>
              </td>
              <td colspan="2">
                <p>6000</p>
              </td>
              <td colspan="2">
                <p>10260</p>
              </td>
              <td colspan="2">
                <p>266470</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>东营</p>
              </td>
              <td>
                <p>207348</p>
              </td>
              <td colspan="2">
                <p>22539</p>
              </td>
              <td colspan="2">
                <p>9076</p>
              </td>
              <td colspan="2">
                <p>10641</p>
              </td>
              <td colspan="2">
                <p>56274</p>
              </td>
              <td colspan="2">
                <p>21490</p>
              </td>
              <td colspan="2">
                <p>10759</p>
              </td>
              <td colspan="2">
                <p>4123</p>
              </td>
              <td colspan="2">
                <p>11959</p>
              </td>
              <td colspan="2">
                <p>354209</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>日照</p>
              </td>
              <td>
                <p>60966</p>
              </td>
              <td colspan="2">
                <p>3001</p>
              </td>
              <td colspan="2">
                <p>4254</p>
              </td>
              <td colspan="2">
                <p>11233</p>
              </td>
              <td colspan="2">
                <p>103932</p>
              </td>
              <td colspan="2">
                <p>30228</p>
              </td>
              <td colspan="2">
                <p>7256</p>
              </td>
              <td colspan="2">
                <p>7196</p>
              </td>
              <td colspan="2">
                <p>11862</p>
              </td>
              <td colspan="2">
                <p>239928</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>青岛</p>
              </td>
              <td>
                <p>143187</p>
              </td>
              <td colspan="2">
                <p>6865</p>
              </td>
              <td colspan="2">
                <p>5069</p>
              </td>
              <td colspan="2">
                <p>21137</p>
              </td>
              <td colspan="2">
                <p>326750</p>
              </td>
              <td colspan="2">
                <p>19999</p>
              </td>
              <td colspan="2">
                <p>2992</p>
              </td>
              <td colspan="2">
                <p>2999</p>
              </td>
              <td colspan="2">
                <p>10400</p>
              </td>
              <td colspan="2">
                <p>539398</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>烟台</p>
              </td>
              <td>
                <p>179705</p>
              </td>
              <td colspan="2">
                <p>10554</p>
              </td>
              <td colspan="2">
                <p>17457</p>
              </td>
              <td colspan="2">
                <p>9872</p>
              </td>
              <td colspan="2">
                <p>73148</p>
              </td>
              <td colspan="2">
                <p>1095</p>
              </td>
              <td colspan="2">
                <p>3394</p>
              </td>
              <td colspan="2">
                <p>3973</p>
              </td>
              <td colspan="2">
                <p>10634</p>
              </td>
              <td colspan="2">
                <p>309832</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>威海</p>
              </td>
              <td>
                <p>72995</p>
              </td>
              <td colspan="2">
                <p>27988</p>
              </td>
              <td colspan="2">
                <p>13020</p>
              </td>
              <td colspan="2">
                <p>10293</p>
              </td>
              <td colspan="2">
                <p>52844</p>
              </td>
              <td colspan="2">
                <p>131650</p>
              </td>
              <td colspan="2">
                <p>15832</p>
              </td>
              <td colspan="2">
                <p>5200</p>
              </td>
              <td colspan="2">
                <p>10582</p>
              </td>
              <td colspan="2">
                <p>340404</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>合计</p>
              </td>
              <td>
                <p>1873911</p>
              </td>
              <td colspan="2">
                <p>162432</p>
              </td>
              <td colspan="2">
                <p>142528</p>
              </td>
              <td colspan="2">
                <p>242345</p>
              </td>
              <td colspan="2">
                <p>1375494</p>
              </td>
              <td colspan="2">
                <p>516632</p>
              </td>
              <td colspan="2">
                <p>87137</p>
              </td>
              <td colspan="2">
                <p>58588</p>
              </td>
              <td colspan="2">
                <p>229321</p>
              </td>
              <td colspan="2">
                <p>4912929</p>
              </td>
            </tr>
          </table>